import React, { Component } from 'react';
import { MovieList } from './../components/Movie';
import { connect } from 'react-redux';
import {
  load as loadMovies
} from './../redux/modules/movies';
import Link from 'next/link';
import Head from 'next/head';

class Index extends Component {
  static async getInitialProps(ctx) {
    const { query, store } = ctx;
    await store.dispatch(loadMovies(query.search));
    return {
      ...query,
      movieLists: store.getState().movies.movieLists
    };
  }

  render() {
    const { search, movieLists } = this.props;
    return (
      <div>
        <Head>
          <title>Search results for: {search}</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <Link href={{ pathname: '/', query: { search: 'batman' } }}><a>Batman</a></Link>
        {' | '}
        <Link href={{ pathname: '/', query: { search: 'superman' } }}><a>Superman</a></Link>
        <MovieList movieLists={movieLists} query={search} />
      </div>
    );
  }
}

export default Index;