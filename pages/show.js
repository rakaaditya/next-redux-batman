import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    loadMovie
} from './../redux/modules/movies';
import Head from 'next/head';
import { MovieCard } from '../components/Movie';

class Show extends Component {
  static async getInitialProps(ctx) {
    const { query, store } = ctx;
    await store.dispatch(loadMovie(query.id));
    return {
      ...query,
      movieDetail: store.getState().movies.movieDetail
    };
  }

  render() {
    const { movieDetail } = this.props;
    return (
      <div>
        <Head>
          <title>{movieDetail && movieDetail.loaded ? movieDetail.data.name : 'Detail'}</title>
        </Head>
        {movieDetail && movieDetail.loaded && <MovieCard show={movieDetail.data} />}
        {movieDetail && movieDetail.loading && <p>Loading...</p>}
      </div>
    );
  }
}

export default connect(state => ({
    movieDetail: state.movies.movieDetail,
  }), { loadMovie })(Show);