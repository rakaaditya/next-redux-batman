import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  load as loadMovies
} from './../../redux/modules/movies';
import { MovieCard } from './';
import styles from './movie.scss';

class MovieList extends Component {
  static async getInitialProps(ctx) {
    console.log(ctx);
  }

  componentWillMount() {
    const { query, loadMovies } = this.props;
    loadMovies(query);
  }

  componentDidUpdate(prevProps) {
    // const { query, loadMovies } = this.props;
    // if(query !== prevProps.query) {
    //   loadMovies(query);
    // }
  }

  render() {
    const { query, movieLists } = this.props;
    return (
      <div>
        <h1>Movie List for {query}</h1>
        <div className={styles.wrapper}>
          {movieLists && movieLists.loaded && (
            movieLists.data.map(movie => {
              return <MovieCard key={movie.show.id} {...movie} />
            })
          )}
          {movieLists && movieLists.loading && <p>Loading...</p>}
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  movieLists: state.movies.movieLists,
}), { loadMovies })(MovieList);