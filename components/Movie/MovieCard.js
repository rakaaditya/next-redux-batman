import React, { Component } from 'react';
import styles from './movie.scss';
import Link from 'next/link';

const Cover = (props) => {
  const { show: { image } } = props;
  return (
    <div>
      <img className={styles.coverImage} src={image.medium} />
    </div>
  );
}

const Content = (props) => {
  const { show: { name } } = props;
  return (
    <div className={styles.content}>
      {name}
    </div>
  );
}

class MovieCard extends Component {
  render() {
    const { show: { id } } = this.props;
    return (
      <Link href={{ pathname: '/show', query: { id: id } }}>
        <div className={styles.card}>
          <Cover {...this.props} />
          <Content {...this.props} />
        </div>
      </Link>
    );
  }
}

export default MovieCard;