import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './modules';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

const initialState = {};
const enhancers = []
const client = axios.create({
  responseType: 'json'
});
const middleware = [
  thunk,
  axiosMiddleware(client)
];

if (process.env.NODE_ENV === 'development' && typeof window !== 'undefined') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const initStore = (initialState) => {
  return createStore(
    rootReducer, 
    initialState,
    composedEnhancers
  );
};

export default initStore;
