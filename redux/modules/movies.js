export const LOAD = 'movies/LOAD';
export const LOAD_SUCCESS = 'movies/LOAD_SUCCESS';
export const LOAD_FAIL = 'movies/LOAD_FAIL';
export const LOAD_DETAIL = 'movies/detail/LOAD';
export const LOAD_DETAIL_SUCCESS = 'movies/detail/LOAD_SUCCESS';
export const LOAD_DETAIL_FAIL = 'movies/detail/LOAD_FAIL';

const initialState = {
  movieLists: {
    loading: false,
    loaded: false,
    data: []
  },
  movieDetail: {
    loading: false,
    loaded: false,
    data: []
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD:
    return {
      movieLists: {
        ...state.movieLists,
        loading: true,
        loaded: false
      }
    }

    case LOAD_SUCCESS:
    return {
      movieLists: {
        ...state.movieLists,
        loading: false,
        loaded: true,
        data: action.payload.data
      }
    }

    case LOAD_FAIL:
    return {
      movieLists: {
        ...state.movieLists,
        loading: false,
        loaded: false,
        error: {
          body: action.error.response ? action.error.response.data : null,
          header: action.error.response ? action.error.response.headers : null,
        }
      }
    }

    case LOAD_DETAIL:
    return {
      movieDetail: {
        ...state.movieDetail,
        loading: true,
        loaded: false
      }
    }

    case LOAD_DETAIL_SUCCESS:
    return {
      movieDetail: {
        ...state.movieDetail,
        loading: false,
        loaded: true,
        data: action.payload.data
      }
    }

    case LOAD_DETAIL_FAIL:
    return {
      movieDetail: {
        ...state.movieDetail,
        loading: false,
        loaded: false,
        error: {
          body: action.error.response ? action.error.response.data : null,
          header: action.error.response ? action.error.response.headers : null,
        }
      }
    }

    default:
    return state
  }
}

export function load(query) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    payload: {
      request:{
        method: 'GET',
        url: `https://api.tvmaze.com/search/shows?q=${query}`
      }
    }
  }
}

export function loadMovie(id) {
  return {
    types: [LOAD_DETAIL, LOAD_DETAIL_SUCCESS, LOAD_DETAIL_FAIL],
    payload: {
      request:{
        method: 'GET',
        url: `https://api.tvmaze.com/shows/${id}`
      }
    }
  }
}